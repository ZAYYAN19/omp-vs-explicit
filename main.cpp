#include <SFML/Graphics.hpp>
#include "FlockSimulation.h"
#include "SceneManager.h"
#include <iostream>
#include <memory>
#include <limits>
#include <map>
#include <chrono>
#include <omp.h>
#include <string>
#include "Runnable.h"
#include "Scene.h"
#include "FlockSimulationScene.h"
#include "ParallelData.h"
#include "ConvexHullScene.h"
#include "BFSScene.h"
#include "ConvexHull.h"
#include "BFSRun.h"

struct Param
{
    int value;
    int lower_bound;
    int upper_bound;
};

bool ParseArgs(int argc, char* argv[], std::map<std::string,Param>& param_defaults, int hull_max )
{
    std::vector<int> object_count_defaults 
    { 1000, hull_max, 2000, 10000, 10000000, 5000000 };

    for(int i = 0; i < argc; i++)
    {
        std::string arg = argv[i];
        auto it = param_defaults.find(arg);
        if( it != param_defaults.end() )
        {
            if( i + 1 < argc )
            {
                // the argument must be an positive integer - all chars must be digits
                std::string arg_value = argv[i+1];
                if( std::all_of(arg_value.begin(),arg_value.end(),::isdigit) )
                {
                    const std::string& param_name = (*it).first;
                    const Param& par = param_defaults[param_name];
                    int value = std::stoi( arg_value );

                    if( value >= par.lower_bound && value <= par.upper_bound )
                    {
                        param_defaults[param_name].value = std::stoi(arg_value);
                    }
                    else
                    {
                        // simple exiting withour error throwing is fine - stack winding uneccessay - 
                        std::cerr << param_name << " must be in the range (" << par.lower_bound << "," << par.upper_bound << ")\n";
                        return false;
                    }
                }
                else
                {
                    std::cerr   << "Command line argument format incorrect: non positive integer value - see ReadMe for examples\n";
                    return false;
                }
            }
            else
            {
                std::cerr   << "Command line argument format incorrect: missing value - see ReadMe for examples\n";
                return false;
            }
        }
    }
    // if object count not supplied by argument then
    if( param_defaults["-n"].value == 0 )
    {
        // object count is a special case as it depends on both the algorithm and whether the program is being benchmarked
        // the first 3 elements are the defaults for the visual program the last three are the defaults for the benchmarks
        param_defaults["-n"].value = object_count_defaults[(param_defaults["-algo"].value-1)+
                                                            3*param_defaults["-benchmark"].value];
    }

    // hack to avoid print statements on profiled runs
    if( param_defaults["-out"].value == 0 )
        std::cout.setstate(std::ios_base::badbit);



    if( param_defaults["-lib"].value == 2 && 
        param_defaults["-algo"].value == 1 && 
        (((param_defaults["-threads"].value & (param_defaults["-threads"].value - 1)) != 0) ||
        param_defaults["-threads"].value == 1))
    {
        std::cerr << "Flocking simulation using C++ threading only runs on threads that are a power of 2" << std::endl;
        return false;
    }
    return true;
}
int main(int argc, char* argv[])
{
    // hardware concurrency
    int avaliable_threads = omp_get_max_threads();

    // default values for parameters that arent supplied
    std::map<std::string,Param> param_defaults
    {
        {"-threads", { avaliable_threads, 1, std::numeric_limits<int>::max() }},
        {"-lib", { 1, 1, 2 }},
        {"-benchmark", { 0, 0, 1 }},
        {"-algo", { 1, 1, 3 }},
        {"-n", { 0, 1, std::numeric_limits<int>::max()}},
        {"-out", { 1, 0, 1} }
    };

    if( !ParseArgs(argc, argv, param_defaults, 50*avaliable_threads ) )
        return 1;

    ParallelData data { param_defaults["-n"].value,
                        param_defaults["-threads"].value,
                        param_defaults["-lib"].value };

    if( param_defaults["-benchmark"].value == 0 )
    {
        // visual stuff - begin scene
        std::unique_ptr<IScene> scene;
        switch ( param_defaults["-algo"].value )
        {
            case 1:
                scene = std::make_unique<FlockSimulationScene>(data);
                break;
            case 2:
                scene = std::make_unique<ConvexHullScene>(data);
                break;
            case 3:
                scene  = std::make_unique<BFSScene>(data);
                break;
        }
        auto manager = std::make_unique<SceneManager>(scene);
        manager->Run(); 
    }
    else if(param_defaults["-benchmark"].value == 1 )
    {
        // begin benchmark 
        std::unique_ptr<Runnable> program;
        switch ( param_defaults["-algo"].value )
        {
            case 1:
                program = std::make_unique<FlockSimulation>( data );
                break;
            case 2:
                program = std::make_unique<ConvexHull>( data );
                break;
            case 3:
                program = std::make_unique<BFSRun>( data );
                break;
        }

        // start benchmark run - will use GBenchmark later but for now this is just fine
        auto start_time = std::chrono::high_resolution_clock::now();
        program->Run();
        auto end_time = std::chrono::high_resolution_clock::now();

        auto run_time = std::chrono::duration_cast<std::chrono::milliseconds>( end_time - start_time );
        program->OutputStats();
        std::cout << std::endl << "Time taken to run: " << run_time.count() << "ms" << std::endl;
    }

    return 0;

}